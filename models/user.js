const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema();

const UserSchema = mongoose.Schema({
    fullname: {type: String},
    username : {type:String},
    password: {type:String}
});

UserSchema.methods.encryptPassword = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};
UserSchema.methods.comparePassword = function(userPassword, cb) {
    bcrypt.compare(userPassword, this.password, (err, isMatch) => {
        if(err) throw err;
        cb(null, isMatch);
    });
}
module.exports = mongoose.model('User', UserSchema);