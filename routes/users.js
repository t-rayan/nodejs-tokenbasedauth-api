var express = require('express');
var router = express.Router();
const User = require('../models/user');
const config = require('../config');
const jwt = require('jsonwebtoken');
const auth = require('../auth');


/* GET users listing. */
router.get('/', auth, function (req, res, next) {
  User.find((err, users) => {
    if (err) throw err;
    res.json(users);
  });
});

router.post('/register', (req, res) => {

  let fullname = req.body.fullname;
  let password = req.body.password;
  let username = req.body.username;

  if (!fullname || !username || !password) {
    res.json({
      success: false,
      message: 'All fields are required'
    });
  } else {
    User.findOne({ username: username }, (err, user) => {
      if (err) throw err;
      if (user) {
        res.json({
          success: false,
          message: 'User already exists'
        });
      } else {
        var newUser = new User();
        newUser.fullname = fullname;
        newUser.username = username;
        newUser.password = newUser.encryptPassword(password);
        newUser.save((err) => {
          if (err) throw err;
          res.json({
            success: true,
            message: 'Registration Completed',
            data: newUser
          });
        });
      }
    })
  }

});

router.post('/login', (req, res, next) => {
  //find the user
  username = req.body.username;
  password = req.body.password;

  if (!username || !password) {
    res.json({
      success: false,
      message: 'All the fields are required'
    });
  } else {
    User.findOne({ username: username }, (err, user) => {
      if (err) throw err;
      if (!user) {
        res.json({ success: false, message: 'Incorrect Credentials' });
      } else if (user) {
        user.comparePassword(password, (err, isMatch) => {
          if (isMatch && !err) {
            var token = jwt.sign(user.toJSON(), config.secret, {
              expiresIn: '1h'
            });
            res.json({
              success: true,
              data: { 'userId': user._id, 'username': user.username, 'name': user.fullname },
              token: token
            })
          } else {
            res.json({ success: false, message: 'Incorrect Credentials' })
          }
        })
      }
    })
  }
});

//auth middleware function
/*
function authenticate(req, res, next) {
  //check header or ul parameters or post parameters for token
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, config.secret);
    req.userData = decoded;
    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed'
    });
  }
}*/

module.exports = router;
