var express = require('express');
var router = express.Router();
const user = require('./users');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ message: 'This is a booking api' });
});

router.get('/projects', (req, res, next) => {
  res.json({ message: 'Projects'})
})
module.exports = router;
