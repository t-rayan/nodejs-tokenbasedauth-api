function auth(req, res, next) {
    //check header or ul parameters or post parameters for token
    try {
      const token = req.headers.authorization.split(" ")[1];
      const decoded = jwt.verify(token, config.secret);
      req.userData = decoded;
      next();
    } catch (error) {
      return res.status(401).json({
        message: 'Auth failed'
      });
    }
  }

  module.exports = auth;